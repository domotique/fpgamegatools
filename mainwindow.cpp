#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include <QMessageBox>
#include <QTreeView>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setWindowTitle("FPGA MEGA TOOLS");

 //   QFileSystemModel *model = new QFileSystemModel;
  //      QTreeView *tree = new QTreeView(splitter);
  //      tree->setModel(model);

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{

}

void MainWindow::on_pushButton_multiplication_clicked()
{

    ui->plainTextEdit->clear();

    //    int j =  ui->centralWidget->spinBox->value();
    int nbr_iteration_max = ui->spinBox->value();
    int nbr_bit = ui->spinBox_bits->value();
    int i = 0 ;
    int j = 0 ;
    double division_reel = 0.0 ;
    bool ok ;
    QString resultat ;
    double nombre_division = ui->lineEdit->text().toDouble(&ok);
    if (nombre_division > 1.0){
        while(nombre_division > pow(2.0,i) ){
            i++;
            qDebug() << "---------- i --------- "<< i;
        }
        while(j<nbr_iteration_max and nombre_division !=0.0 and i <= nbr_bit and i >= -nbr_bit){
            //for (int i=1;i<nbr_iteration;i++) {
            if (pow(2.0,i) <= nombre_division){
                nombre_division = nombre_division-(pow(2.0,i));
                division_reel = division_reel + (pow(2.0,i)) ;
                resultat.append(" + ");
                resultat.append(QString::number(pow(2.0,i)));

                qDebug() << "---------- i --------- "<< i;
                qDebug() << "power " << 1.0/pow(2.0,i);

                qDebug() << "reste "<< nombre_division ;
                j++;
            }
            i--;
        }
    }
    else {
        while(j<nbr_iteration_max and nombre_division !=0.0  and i <= nbr_bit){
            //for (int i=1;i<nbr_iteration;i++) {
            if (1.0/pow(2.0,i) <= nombre_division){
                nombre_division = nombre_division-(1.0/pow(2.0,i));
                division_reel = division_reel + (1.0/pow(2.0,i)) ;
                resultat.append(" + 1/");
                resultat.append(QString::number(pow(2.0,i)));

                qDebug() << "---------- i --------- "<< i;
                qDebug() << "power " << 1.0/pow(2.0,i);

                qDebug() << "reste "<< nombre_division ;
                j++;
            }
            else {

            }
            i++ ;
        }
    }
    ui->plainTextEdit->appendPlainText(resultat);
    ui->label_precision_result->setText(QString::number(nombre_division*100.0,'f',10) + "%");
    ui->lineEdit_multiplication_reel->setText(QString::number(division_reel,'f',10));


}

void MainWindow::on_actionabout_triggered()
{
    QMessageBox msgBox;
    msgBox.setText("Created by Omar HLIMI for graine d'innovation 31");
    msgBox.exec();
}


void MainWindow::on_pushButton_process_cnt_clicked()
{

calcul_compteur_frequence();

}

void MainWindow::calcul_compteur_frequence(){
    bool ok ;
    int i = 0 ;
    // convertit en fonction de l'unité
    double clk_freq = ui->lineEdit_clk_cnt->text().toDouble(&ok);

    qDebug() << ui->comboBox_clk_cnt_unit->currentText() ;


    if (ui->comboBox_clk_cnt_unit->currentText() == "MHz"){
        clk_freq = clk_freq * 1000000.0 ;
    }
    if (ui->comboBox_clk_cnt_unit->currentText() == "KHz"){
        clk_freq = clk_freq * 1000.0 ;
    }
    qDebug() << "clk_freq " << clk_freq ;
// convertit en fonction de l'unité
    double cnt_freq = ui->lineEdit_freq_cnt->text().toDouble(&ok);
    if (ui->comboBox_freq_cnt_unit->currentText() == "MHz"){
        cnt_freq = cnt_freq * 1000000.0 ;
    }
    if (ui->comboBox_freq_cnt_unit->currentText() == "KHz"){
        cnt_freq = cnt_freq * 1000.0 ;
    }
    qDebug() << "cnt_freq " << cnt_freq ;
    //TODO rajouté une condition de non coherence
    double valeur_compteur = clk_freq/cnt_freq ;
    ui->lineEdit_cnt_value->setText(QString::number((valeur_compteur),'f',2));

    //todo faire le calcul du nombre de bit necessaire

    while(valeur_compteur > pow(2.0,i) ){
        i++;
        qDebug() << "---------- i --------- "<< i;
    }
    ui->label_nbr_bits_cnt->setNum(i);


}

void MainWindow::calcul_frequence(){
    bool ok ;
    double periode = ui->lineEdit_period_cnt->text().toDouble(&ok);
    if (ui->comboBox_period_cnt_unit->currentText() == "ms"){
        periode = periode / 1000.0 ;
    }
    if (ui->comboBox_period_cnt_unit->currentText() == "µs"){
        periode = periode / 1000000.0 ;
    }
    if (ui->comboBox_period_cnt_unit->currentText() == "ns"){
        periode = periode / 1000000000.0 ;
    }
    qDebug() << "periode" << periode ;

    if (periode > 0.001 ){
        ui->comboBox_freq_cnt_unit->setCurrentIndex(0);
        ui->lineEdit_freq_cnt->setText(QString::number(1.0/periode)) ;
        qDebug() << "Hz" ;
    }
    else if (periode > 0.000001 ){
        ui->comboBox_freq_cnt_unit->setCurrentIndex(1);
        ui->lineEdit_freq_cnt->setText(QString::number(1/(periode*1000))) ;
        qDebug() << "kHz" ;
    }
    else if (periode > 0.000000001 ){
        ui->comboBox_freq_cnt_unit->setCurrentIndex(2);
        ui->lineEdit_freq_cnt->setText(QString::number(1/(periode*1000000))) ;
        qDebug() << "MHz" ;
    }
    else{
        ui->comboBox_freq_cnt_unit->setCurrentIndex(3);
        ui->lineEdit_freq_cnt->setText(QString::number(1/(periode*1000000000))) ;
        qDebug() << "GHz" ;
    }
}

void MainWindow::calcul_period(){
    bool ok ;
    double frequence = ui->lineEdit_freq_cnt->text().toDouble(&ok);
    if (ui->comboBox_freq_cnt_unit->currentText() == "MHz"){
        frequence = frequence * 1000000.0 ;
        qDebug() << "MHz" ;
    }
    if (ui->comboBox_freq_cnt_unit->currentText() == "KHz"){
        frequence = frequence * 1000.0 ;
        qDebug() << "KHz" ;
    }

    if (frequence <= 1.0 ){
        ui->comboBox_period_cnt_unit->setCurrentIndex(0);
        ui->lineEdit_period_cnt->setText(QString::number(1.0/frequence)) ;
        qDebug() << "s" ;
    }
    else if (frequence <= 1000.0 ){
        ui->comboBox_period_cnt_unit->setCurrentIndex(1);
        ui->lineEdit_period_cnt->setText(QString::number(1000.0/frequence)) ;
        qDebug() << "ms" ;
    }
    else if (frequence <= 1000000.0 ){
        ui->comboBox_period_cnt_unit->setCurrentIndex(2);
        ui->lineEdit_period_cnt->setText(QString::number(1000000.0/frequence)) ;
        qDebug() << "µs" ;
    }
    else{
        ui->comboBox_period_cnt_unit->setCurrentIndex(3);
        ui->lineEdit_period_cnt->setText(QString::number(1000000000.0/frequence)) ;
        qDebug() << "ns" ;
    }
}


void MainWindow::on_lineEdit_freq_cnt_returnPressed()
{
    calcul_period();
    calcul_compteur_frequence();
}

void MainWindow::on_lineEdit_period_cnt_returnPressed()
{
    calcul_frequence();
    calcul_compteur_frequence();
}

void MainWindow::on_comboBox_period_cnt_unit_currentIndexChanged(int index)
{
    calcul_frequence();
    calcul_compteur_frequence();
}

void MainWindow::on_comboBox_freq_cnt_unit_currentIndexChanged(int index)
{
    calcul_period();
    calcul_compteur_frequence();
}
