#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void calcul_compteur_frequence();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_multiplication_clicked();

    void on_actionabout_triggered();

    void on_pushButton_process_cnt_clicked();

    void calcul_frequence();

    void calcul_period();

    void on_lineEdit_freq_cnt_returnPressed();

    void on_lineEdit_period_cnt_returnPressed();

    void on_comboBox_period_cnt_unit_currentIndexChanged(int index);

    void on_comboBox_freq_cnt_unit_currentIndexChanged(int index);

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
